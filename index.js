const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
  }));
let globalArray = [];

app.post('/candidates', function(req, res) {
   const {body} = req;
   console.log(body);

    const idCheck = globalArray.some((ele,index,array)=> ele.id == body.id);
    const skillCheck = body.skills.some((ele,index)=> body.skills.indexOf(ele) !== index);

    if(!idCheck && !skillCheck){
        globalArray.push(body);
    } else {
        res.sendStatus(409);
    }
    res.send(200);
   
});
app.get('/candidates/search', function(req, res) {
   const {query} = req;
    if(!(Object.entries(query).length === 0 && query.constructor === Object)){
        let  str = query.skills.replace(/["]/g, "");;
        let queryArray = str.split(',');
        let equalMatchArr;
        if(!queryArray.length || queryArray == ''){
            res.send(globalArray);
        }
        if(globalArray.length){
            let outputArr = globalArray.filter((element)=>{
                return (queryArray.every((e) => element.skills.includes(e)));
            });
            if(outputArr.length == ""){
                equalMatchArr = globalArray.filter((element)=>{
                    return (queryArray.some((e) => element.skills.includes(e)));
                }); 
                res.send(equalMatchArr);
            } else {
                res.send(outputArr);
            }
        } else {
            res.send(404);
        }
    } else {
        if(globalArray.length){
        res.send(globalArray);
        }
        else {
            res.send(404);
        }
    }

});

module.exports = app;

console.log("listening on 9090")

app.listen(9090);
