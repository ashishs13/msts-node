const expect = require('chai').expect;
const request = require('supertest');
const should = require('should');

const app = require('../../index')
describe('GET /candidates',()=>{

    it('Ok, getting candidates with no data ',(done)=>{
        request(app).get('/candidates/search')
        .end(function(err,res){
            expect(err).to.be.null;
            const body= res.body;
            console.log(body);
            expect(Object.entries(body).length).to.equal(0);
            res.should.have.property('status', 404);
            done();
        });
    });
});
describe('POST /candidates',()=>{

    it('Ok, creating a new candidates ',(done)=>{
        request(app).post('/candidates/')
        .send({
            "id": "person4",
            "name": "Amy Winehouse",
            "skills": [ "nodejs", "mongodb", "redis", "java" ]
            })
        .end(function(err,res){
            expect(err).to.be.null;
            res.should.have.property('status', 200);
            done();
        });
    });
});

describe('GET /candidates',()=>{

    it('Ok, getting candidates with single data ',(done)=>{
        request(app).get('/candidates/search')
        .end(function(err,res){
            expect(err).to.be.null;
            const body= res.body;
            console.log(body);
            expect(Object.entries(body).length).to.equal(1);
            res.should.have.property('status', 200);
            done();
        });
    });
});

