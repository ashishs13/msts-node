MSTS test

RUN PROJECT

Install dependencies
    npm Install


Start API
    npm Start

Run test
    npm run test


The server will start on port no 9090.


Post Candidates

1> Url for posting the data
    localhost:9090/candidates/

2> body of the data should look like 
    {
        "id": "person1",
        "name": "Amy Winehouse",
        "skills": [ "nodejs", "mongodb"]
    }

3> If candidates posted successfully. It will send HTTP status code 200 "ok".

3> If two post request has the same ID. It will send HTTP status code 409 "conflict".

4> If skills array contains duplicate skills. It will send HTTP status code 409 "conflict".


Get Candidates

1> Url for getting the candidates.
    localhost:9090/candidates/search

    The above URL will fetch all the candidates. With status code 200.

2> If no candidates present. It will send HTTP status code 400 "not found".

3>Get candidates with most skills, Url will be 
    localhost:9090/candidates/search?skills="mongodb","redis"

    The above URL will get the topmost candidates whose skills are as mentioned in the query string.
    Result for the above URL will be 

    [
    {
        "id": "person4",
        "name": "Amy Winehouse",
        "skills": [
            "nodejs",
            "mongodb",
            "redis",
            "java"
        ]
    }
]

with status code 200.
